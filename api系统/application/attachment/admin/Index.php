<?php
namespace app\attachment\admin;
use app\system\admin\Admin;

class Index extends Admin
{
    protected $oneModel = '';//模型名称[通用添加、修改专用]
    protected $oneTable = '';//表名称[通用添加、修改专用]
    protected $oneAddScene = '';//添加数据验证场景名
    protected $oneEditScene = '';//更新数据验证场景名

    public function index()
    {
        return $this->fetch();
    }
}