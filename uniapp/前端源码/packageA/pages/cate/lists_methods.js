const MyApp = getApp();
const globalData = MyApp.globalData;
import util from '@/utils/utils.js';
export default {
    computed: {
        i18n() {
            return globalData.$t;
        }
    },
    data() {
        return {
            adSwitchKg: globalData.adSwitchKg || uni.getStorageSync('sys_config').ad_switch,
            oneAd: MyApp.getOneAd('common_ad'),
            tabIndex: 0,
            tarbarObj: globalData.tarbarObj,
            typelist: globalData.typelist,
            imageUrl: this.$config.imageUrl,
            flowList: [],
            loadStatus: 'loadmore',
            postMap: {
                page: 1,
                type: 0
            },
            isEnd: false,
            loadText: {
                loadmore: '上拉加载更多',
                loading: '加载中，请稍候',
                nomore: '没有了哦~'
            },
            scrollTop: 0,
            isShowxx: false
        };
    },
    onShow() {
        let _this = this;
        _this.tabIndex = uni.getStorageSync('tabIndex') || 0;
        let config = uni.getStorageSync('sys_config');
        _this.isShowxx = isNaN(parseInt(config.show_xx)) ? true : parseInt(config.show_xx);
    },
    onPageScroll(e) {
        this.scrollTop = e.scrollTop;
    },
    onLoad(option) {
        this.postMap.type = option.id;
    },
    onPullDownRefresh() {
        this.postMap.page = 1;
        this.isEnd = false;
        this.loadStatus = 'loading';
        this.$refs.uWaterfall.clear();
        this.getCateList();
    },
    onReachBottom() {
        this.getMore();
    },
    onReady() {
        this.getCateList();
    },
    methods: {
        getCateList() {
            let _this = this;
            if (!_this.isEnd) {
                _this.loadStatus = 'loading';
                MyApp.showLoading(_this.$refs.loading);
                _this.$http.post('/videos/videos/getTypeVideoLists', _this.postMap).then(res => {
                    _this.isChange = false;
                    uni.stopPullDownRefresh();
                    MyApp.closeLoading(_this.$refs.loading);
                    if (res.data.list.length > 0) {
                        res.data.list.forEach((v, k) => {
                            _this.flowList.push(v);
                        });
                        _this.loadStatus = 'loadmore';
                    } else {
                        _this.isEnd = true;
                        _this.loadStatus = 'nomore';
                    }
                }).catch(res => {
					MyApp.closeLoading(_this.$refs.loading);
				});
            }
        },
        goDetail(item) {
            MyApp.godetail(item.vod_id);
        },
        getMore: util.throttle(function(e) {
            this.postMap.page = this.postMap.page + 1;
            this.getCateList();
        }, 800),
    }
};