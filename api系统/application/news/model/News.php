<?php
namespace app\news\model;

use app\common\model\Base;

class News extends Base
{
    public function initialize()
    {
        parent::initialize();
        if (!isset($this->modelKey) || empty($this->modelKey) || cache('modelKey') != $this->modelKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>711]));
        }
    }

    public function getImgAttr($v, $d)
    {
        return strstr($v, ',') ? implode(',', $v) : $v;
    }
}