<?php

namespace app\banner\admin;

use app\system\admin\Admin;
use app\banner\model\Banner as BannerModel;
use app\banner\model\Ad as AdModel;

class Banner extends Admin
{
    protected $oneModel = 'Banner'; //模型名称[通用添加、修改专用]
    protected $oneTable = ''; //表名称[通用添加、修改专用]
    protected $oneAddScene = ''; //添加数据验证场景名
    protected $oneEditScene = ''; //更新数据验证场景名

    public function initialize()
    {
        parent::initialize();
        $this->BannerModel = new BannerModel();
        $this->AdModel = new AdModel();
    }
    /**
     * 轮播图
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $map    = $data = [];
            $data   = input();
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;

            $data = $this->BannerModel->getList($map, $page, $limit);
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        $data = [];
        $data = input();
        if ($this->request->isPost()) {
        	$img = is_array($data['img']) ? implode(',', $data['img']) : $data['img'];
            $_data = [
                'id' => $data['id'],
                'title' => $data['title'],
                'img' => isset($data['img_url']) && !empty($data['img_url']) ? $data['img_url'] : $img,
                'vid' => $data['vid'],
                'isad' => $data['isad'],
                'ad_url' => $data['ad_url'],
            ];
            if ($data['isad'] == 2) {
                $_data['type'] = 'mpad';
            }
            if (false !== $this->BannerModel->isUpdate($data['id'] > 0 ? true : false)->save($_data)) {
                return $this->success('保存成功');
            }
            return $this->error('保存失败' . $this->BannerModel->getError());
        }
        $formData = $this->BannerModel->where('id', $id)->find();
        $this->assign('formData', $formData ? $formData->toArray() : []);
        return $this->fetch('form');
    }



    /**
     * 广告列表
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function ad()
    {
        if ($this->request->isAjax()) {
            $map    = $data = [];
            $data   = input();
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;

            $data = $this->AdModel->getList($map, $page, $limit);
            return $this->success('保存成功', '', $data);
        }
        $this->assign('type', 1);
        return $this->fetch('ad');
    }
    /**
     * 添加广告
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function editAd($id = 0)
    {
        $data = [];
        $data = input();
        $_type = [
            [
                'label' => '启动页',
                'value' => 1
            ],
            [
                'label' => '首页',
                'value' => 2
            ],
            [
                'label' => '播放页弹窗',
                'value' => 3
            ],
            [
                'label' => '播放页底部',
                'value' => 4
            ],
            [
                'label' => '视频播放前',
                'value' => 5
            ],
            [
                'label' => '播放前激励',
                'value' => 6
            ],
        ];
        $_options = [
            [
                'label' => '自定义广告',
                'options' => [
                    [
                        'label' => '自定义图片',
                        'value' => 1
                    ],
                    [
                        'label' => '自定义视频',
                        'value' => 2
                    ],
                ],
            ],
            [
                'label' => 'WeChat小程序广告',
                'options' => [
                    [
                        'label' => 'WeChat-小程序BANNER广告',
                        'value' => 3
                    ],
                    [
                        'label' => 'WeChat-小程序视频广告',
                        'value' => 4
                    ],
                    [
                        'label' => 'WeChat-小程序插屏广告',
                        'value' => 5
                    ],
                    [
                        'label' => 'WeChat-小程序格子广告',
                        'value' => 6
                    ],
                    [
                        'label' => 'WeChat-小程序视频贴片广告',
                        'value' => 7
                    ],
                    [
                        'label' => 'WeChat-小程序激励式广告',
                        'value' => 8
                    ],
                    [
                        'label' => 'WeChat-小程序原生模板广告',
                        'value' => 9
                    ],
                ],
            ],

        ];
        $type = isset($data['type']) && !empty($data['type']) ? $data['type'] : 1;
        if ($this->request->isAjax()) {
        	$img = is_array($data['img']) ? implode(',', $data['img']) : $data['img'];
            $_data = [
                'id' => $data['id'] > 0 ? $data['id'] : 0,
                'title' => $data['title'],
                'img' => $img,
                'url' => $data['url'],
                'v_url' => $data['v_url'],
                'desc' => isset($data['desc']) ? $data['desc'] : '',
                'type' => $data['type'],
                'ad_type' => $data['ad_type'],
                'group_id' => $data['group_id'],
                
            ];
            if (false !== $this->AdModel->isUpdate($data['id'] > 0 ? true : false)->save($_data)) {
                return $this->success('保存成功');
            }
            return $this->error('保存失败' . $this->AdModel->getError());
        }
        $formData = $this->AdModel->where('id', $id)->find();
        $this->assign('formData', $formData ? $formData->toArray() : []);
        $this->assign('type', $type);
        $this->assign('options', $_options);
        $this->assign('type', $_type);
        return $this->fetch('adform');
    }
}
