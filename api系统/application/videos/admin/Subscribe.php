<?php
namespace app\videos\admin;
use app\system\admin\Admin;
use app\videos\model\Subscribe as SubscribeModel;

class Subscribe extends Admin
{
    protected $oneModel = 'Subscribe';//模型名称[通用添加、修改专用]
    protected $oneTable = '';//表名称[通用添加、修改专用]
    protected $oneAddScene = '';//添加数据验证场景名
    protected $oneEditScene = '';//更新数据验证场景名

    public function initialize()
    {
        parent::initialize();
        $this->SubscribeModel = new SubscribeModel();
    }

    public function index()
    {
        if ($this->request->isAjax()) {
            $map    = $data = [];
            $data   = input();
            $page   = isset($data['page']) ? $data['page'] : 1;
            $limit  = isset($data['limit']) ? $data['limit'] : 15;

            $data = $this->SubscribeModel->getList($map, $page, $limit);
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }
}