<?php
namespace think;
define('DS', DIRECTORY_SEPARATOR);
require __DIR__ . '/../library/Base.php';
require __DIR__ . '/../thinkphp/base.php';
if (!is_file('./../install.lock')) {
    define('ENTRANCE','one');
    Container::get('app')->bind('install')->run()->send();
} else {
    Container::get('app')->run()->send();
}
