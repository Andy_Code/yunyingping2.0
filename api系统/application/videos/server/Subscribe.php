<?php

namespace app\videos\server;

use app\common\server\Service;
use app\videos\model\Subscribe as SubscribeModel;

class Subscribe extends Service
{

    public function initialize()
    {
        parent::initialize();
        if (!isset($this->serviceKey) || empty($this->serviceKey) || cache('serviceKey') != $this->serviceKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>712]));
        }
        $this->SubscribeModel = new SubscribeModel();
    }
    /**
     * 记录订阅信息
     *
     * @param [type] $data
     * @param [type] $user
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function submit($data, $user) {
        $data['uid'] = $user['id'];
        $data['vod_actor'] = mb_substr($data['vod_actor'], 0, 15) . '...';
        $map = [];
        $map[] = ['uid', 'eq', $user['id']];
        if ($one = $this->SubscribeModel->where($map)->find()) {
            $one->vod_id = $data['vod_id'];
            $one->vod_title = $data['vod_title'];
            $one->vod_time = $data['vod_time'];
            $one->vod_year = $data['vod_year'];
            $one->vod_actor = $data['vod_actor'];
            $one->vod_score = $data['vod_score'];
            $one->status = 0;
            return $one->save();
        }
        return $this->SubscribeModel->allowField(true)->save($data);
    }
    
}
