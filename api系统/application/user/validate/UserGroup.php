<?php
namespace app\user\validate;

use think\Validate;

/**
 * 会员分组验证器
 * @package app\user\validate
 */
class UserGroup extends Validate
{
    //定义验证规则
    protected $rule = [
        'name|等级名称' => 'require|unique:user_group',
        'status|状态设置'  => 'require|in:0,1',
    ];

    //定义验证提示
    protected $message = [
        'name.require' => '请填写等级名称',
        'name.unique' => '等级名称已存在',
        'status.require'    => '请设置等级状态',
    ];
}
