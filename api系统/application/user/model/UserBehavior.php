<?php
namespace app\user\model;

use app\common\model\Base;

/**
 * 会员收藏（关注、点赞等）
 * @package app\user\model
 */
class UserBehavior extends Base
{
    public function initialize()
    {
        parent::initialize();
    }

    public function getTypeTextAttr($v, $d)
    {
        $arr = ['1' => '截图'];
        return $arr[$d['type']];
    }

}