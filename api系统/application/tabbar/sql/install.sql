/*
 sql安装文件*/
CREATE TABLE `one_tabbar`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL DEFAULT 0 COMMENT '导航类型',
  `nav_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '导航名称',
  `nav_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '别名',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '导航图标',
  `active_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '导航选中图标',
  `sort` tinyint(3) NOT NULL DEFAULT 100 COMMENT '排序',
  `jump_type` tinyint(3) NOT NULL DEFAULT 1 COMMENT '跳转类型 1 本小程序 2 其他小程序',
  `jump_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '跳转地址',
  `appid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '其他小程序appid',
  `mp_page` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '其他小程序页面地址',
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '是否显示',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NULL DEFAULT 0,
  `is_default` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT = '底部菜单列表';

CREATE TABLE `one_tabbar_type`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型名',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT = '底部菜单分类';

INSERT INTO `one_tabbar` VALUES (1, 1, '主页', 'Home', '/upload/sys/image/e3/b70dab18457a9791175edf70b104ea.png', '/upload/sys/image/a9/3bba0dd1415729c321e782a7322fa4.png', 3, 1, '/pages/index/index', '', '', 1, 1620287087, 1628090391, 1);
INSERT INTO `one_tabbar` VALUES (2, 1, '搜索', 'Search', '/upload/sys/image/6c/b33c9ecffbab1dbdaa523a80177669.png', '/upload/sys/image/46/540a216ded32faf7565053bcaf2a8e.png', 1, 1, '/pages/search/index', '', '', 1, 1620287113, 1621612143, 0);
INSERT INTO `one_tabbar` VALUES (3, 1, '分类', 'Category', '/upload/sys/image/b2/0227c5190fbaae54ffad921d8a116b.png', '/upload/sys/image/31/45ca1a81f99d71f3189c8ee9007b77.png', 2, 1, '/pages/cate/index', '', '', 1, 1620287128, 1621612034, 0);
INSERT INTO `one_tabbar` VALUES (4, 1, '壁纸', 'Wallpaper', '/upload/sys/image/85/21c1794d48d95dc1970845855f66b9.png', '/upload/sys/image/a0/9340db150022365751a5d6ae1e88ac.png', 4, 1, '/pages/wallpaper/index', '', '', 1, 1620287141, 1621612046, 0);
INSERT INTO `one_tabbar` VALUES (5, 1, '我的', 'Mine', '/upload/sys/image/3d/8ba1d62941c394810c373b37617fe5.png', '/upload/sys/image/e8/fbeca4720515d75d55eac975703184.png', 5, 1, '/pages/user/index', '', '', 1, 1620287141, 1621612053, 0);

INSERT INTO `one_tabbar_type` VALUES (1, '底部菜单', 1, 1620287027, 1620287027);