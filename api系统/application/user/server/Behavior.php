<?php
namespace app\user\server;
use app\common\server\Service;
use app\user\model\UserBehavior as UserBehaviorModel;
use app\user\model\User as UserModel;

class Behavior extends Service{

    public function initialize() {
        parent::initialize();
        if (!isset($this->serviceKey) || empty($this->serviceKey) || cache('serviceKey') != $this->serviceKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>712]));
        }
        $this->UserBehaviorModel = new UserBehaviorModel();
        $this->UserModel = new UserModel();
    }

    public function logs($data, $user)
    {
        if (!empty($user)) {
            // 用户已登录，校验权限
            $member_group_switch = config('commoncfg.member_group_switch');
            $member_group_limit = config('commoncfg.member_group_limit');
            if ($member_group_switch && $user['group_id'] >= $member_group_limit) {
                $data = [
                    'uid' => $user['id'],
                    'type' => $data['type'],
                    'num' => 1,
                    'remark' => $data['page']
                ];
                // 封掉截图的帐号
                if (strstr($data['page'], 'giftCode')) {
                    $this->UserModel->where('id', $user['id'])->setField('status', 0);
                }
                return $this->UserBehaviorModel->save($data);
            }
        }
        return true;
    }

}